<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

return function (App $app) {
    $container = $app->getContainer();

    // $app->get('/[{name}]', function (Request $request, Response $response, array $args) use ($container) {
    //     // Sample log message
    //     $container->get('logger')->info("Slim-Skeleton '/' route");

    //     // Render index view
    //     return $container->get('renderer')->render($response, 'index.phtml', $args);
    // });
        
    $app->get("/informasi", function (Request $request, Response $response){
        $sql = "SELECT SUM(positif)as positif,SUM(sembuh) as sembuh ,SUM(meningal) as meningal,SUM(positif)+SUM(sembuh)+ SUM(meningal) as total FROM informasikasuscovid  ORDER BY tanggal_buat";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        return $response->withJson(["positif" => $result["positif"],"sembuh" => $result["sembuh"],"meningal" => $result["meningal"],"total" => $result["total"]], 200);
    });
    $app->get("/informasiSemua", function (Request $request, Response $response){
        $sql = "SELECT * FROM informasikasuscovid  ORDER BY tanggal_buat";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $response->withJson(["status" => "success", "informasikasuscovid" => $result], 200);
    });
    
    $app->get("/dampak", function (Request $request, Response $response){
        $sql = "SELECT * FROM dampak";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $response->withJson(["status" => "success", "data" => $result], 200);
    });
    $app->get("/gejala", function (Request $request, Response $response){
        $sql = "SELECT * FROM gejalaterinfeksi";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $response->withJson(["status" => "success", "data" => $result], 200);
    });
    $app->get("/cara", function (Request $request, Response $response){
        $sql = "SELECT * FROM carapenyebaran";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $response->withJson(["status" => "success", "data" => $result], 200);
    });
    $app->get("/handsanatizer", function (Request $request, Response $response){
        $sql = "SELECT * FROM handsanatizer";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $response->withJson(["status" => "success", "data" => $result], 200);
    });
    $app->get("/latar", function (Request $request, Response $response){
        $sql = "SELECT * FROM latarbelakang";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $response->withJson(["status" => "success", "data" => $result], 200);
    });
    $app->get("/penangan", function (Request $request, Response $response){
        $sql = "SELECT * FROM penangan";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $response->withJson(["status" => "success", "data" => $result], 200);
    });
    $app->get("/pencegahan", function (Request $request, Response $response){
        $sql = "SELECT * FROM pencegahan";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $response->withJson(["status" => "success", "data" => $result], 200);
    });
    $app->get("/penyebab", function (Request $request, Response $response){
        $sql = "SELECT * FROM penyebab";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $response->withJson(["status" => "success", "data" => $result], 200);
    });
    $app->get("/rumahsakit", function (Request $request, Response $response){
        $sql = "SELECT * FROM rumahsakitrujukan";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $response->withJson(["status" => "success", "data" => $result], 200);
    });
};
